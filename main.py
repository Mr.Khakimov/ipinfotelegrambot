import os
from dotenv import load_dotenv
import json
import requests
import telebot
from telebot import types
import re
import sqlite3
import openpyxl

load_dotenv()
bot_token = os.getenv("BOT_TOKEN")
ipInfoToken = os.getenv("IP_INFO_TOKEN")

bot = telebot.TeleBot(bot_token)

adminId = 545050591

def connect_to_database():
    conn = sqlite3.connect('user_data.db')


    cursor = conn.cursor()

    # cursor.execute('''
    # CREATE TABLE IF NOT EXISTS users (
    #     user_id INTEGER PRIMARY KEY,
    #     username TEXT,
    #     first_name TEXT,
    #     last_name TEXT,
    #     language_code TEXT,
    #     can_join_groups INTEGER,
    #     can_read_all_group_messages INTEGER,
    #     supports_inline_queries INTEGER,
    #     is_premium INTEGER,
    #     added_to_attachment_menu INTEGER,
    #     phone_number TEXT
    # )
    # ''')

    return conn, cursor

def ipInfo(ip_address, myIp):
    if re.match(r'^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$', ip_address):
        try:
            if myIp:
                response = requests.get(f'https://ipinfo.io?token=ff212c88e5a25d')

            else:
                response = requests.get(f'https://ipinfo.io/{ip_address}?token={ipInfoToken}')

            data = json.loads(response.text)
            if 'status' in data:
                if (data['status'] == 404):
                    return f" {data['error']['title']} \n {data['error']['message']}"
                if (data['status'] == 403):
                    bot.send_message(adminId, f"Измени IPINFO TOKEN на боте ipmalumotlari")
                    return f"Ошибка ), Пожалуйста, попробуйте еще раз."
                else:
                    return  "Ошибка ), Пожалуйста, попробуйте еще раз."
            else:
                info = f"Информация об IP-адресе {data['ip']}:\n"
                if 'org' in data:
                    info += f"Организация: {data['org']}\n"
                if 'city' in data:
                    info += f"Город: {data['city']}\n"
                if 'region' in data:
                    info += f"Регион: {data['region']}\n"
                if 'country' in data:
                    info += f"Страна: {data['country']}\n"

                return info
        except Exception as e:
            return f"Произошла ошибка: {str(e)}"
    else:
        return "Неверный формат IP-адреса. Пожалуйста, введите корректный IP."

def is_admin(user_id):
    # Добавьте здесь логику проверки, является ли пользователь администратором
    return user_id == adminId

@bot.message_handler(commands=['admin'])
def admin_menu(message):
    if is_admin(message.chat.id):  # Проверка, является ли пользователь администратором
        markup = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
        item = types.KeyboardButton("Мои пользователи")
        markup.add(item)
        bot.send_message(message.chat.id, "Выберите действие:", reply_markup=markup)

@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
    conn, cursor = connect_to_database()

    user_id = message.from_user.id
    username = message.from_user.username
    first_name = message.from_user.first_name
    last_name = message.from_user.last_name
    phone_number = message.contact.phone_number if message.contact else None
    language_code = message.from_user.language_code
    can_join_groups = message.from_user.can_join_groups
    can_read_all_group_messages = message.from_user.can_read_all_group_messages
    supports_inline_queries = message.from_user.supports_inline_queries
    is_premium = message.from_user.is_premium
    added_to_attachment_menu = message.from_user.added_to_attachment_menu

    cursor.execute(
        'INSERT OR REPLACE INTO users (user_id, username, first_name, last_name, language_code, can_join_groups, can_read_all_group_messages, supports_inline_queries, is_premium, added_to_attachment_menu, phone_number) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)',
        (user_id, username, first_name, last_name, language_code, can_join_groups, can_read_all_group_messages,
         supports_inline_queries, is_premium, added_to_attachment_menu, phone_number))

    conn.commit()

    cursor.close()
    conn.close()

    bot.send_message(message.chat.id, "Привет! Этот бот может определить информацию об IP-адресе.")
    messageSearch = bot.send_message(message.chat.id, "ищем ваш IP...")
    result = ipInfo('8.8.8.8', True)
    bot.delete_message(message.chat.id, messageSearch.message_id)
    bot.send_message(message.chat.id, f"Ваши дынные Ip \n\n {result}")
    bot.send_message(message.chat.id, "Просто отправьте IP-адрес, и я предоставлю информацию о нем. чтобы узнать про ваш Ip адрес введите команду /start  или /myip")

    admin_menu(message)

@bot.message_handler(commands=['myid'])
def send_myid(message):
    bot.send_message(message.chat.id, f"Ваш телеграм ID \n\n {message.chat.id}")


@bot.message_handler(commands=['myip'])
def send_myip(message):
    messageSearch = bot.send_message(message.chat.id, "ищем ваш IP...")
    result = ipInfo('8.8.8.8', True)
    bot.delete_message(message.chat.id, messageSearch.message_id)
    bot.send_message(message.chat.id, f"Ваши дынные Ip \n\n {result}")
    bot.send_message(message.chat.id, "Просто отправьте IP-адрес, и я предоставлю информацию о нем. чтобы узнать про ваш Ip адрес введите команду /start  или /myip")


def get_user_list():
    conn, cursor = connect_to_database()

    # Получите список пользователей из базы данных
    cursor.execute('SELECT user_id, username FROM users')
    user_list = [f"{row[1]} (ID: {row[0]})" for row in cursor.fetchall()]

    cursor.close()
    conn.close()

    return user_list

@bot.message_handler(func=lambda message: message.text == "Мои пользователи")
def send_message_to_users(message):
    if is_admin(message.chat.id):
        user_list = get_user_list()  # Получите список пользователей из базы данных
        if user_list:
            user_list_message = "Список пользователей для отправки сообщения:\n" + "\n".join(user_list)
            bot.send_message(message.chat.id, user_list_message)
            user_data = get_user_data()
            create_excel_file(user_data)
        else:
            bot.send_message(message.chat.id, "Список пользователей пуст.")
    else:
        bot.send_message(message.chat.id, "У вас нет доступа к этой функции.")

@bot.message_handler(func=lambda message: True)
def get_ip_info(message):
    messageSearch = bot.send_message(message.chat.id, "ищем данные...")
    ip_address = message.text
    # Проверка на валидность IP-адреса
    result = ipInfo(ip_address, False)
    bot.delete_message(message.chat.id, messageSearch.message_id)
    bot.reply_to(message, result)


def get_user_data():
    conn = sqlite3.connect('user_data.db')
    cursor = conn.cursor()

    cursor.execute('SELECT * FROM users')
    user_data = cursor.fetchall()

    conn.close()

    return user_data

def send_excel_to_bot(chat_id):
    with open('user_data.xlsx', 'rb') as excel_file:
        bot.send_document(chat_id, excel_file)

def create_excel_file(user_data):
    wb = openpyxl.Workbook()
    ws = wb.active

    # Заголовки столбцов
    headers = ['User ID', 'Username', 'First Name', 'Last Name', 'Language Code', 'Can Join Groups', 'Can Read All Group Messages', 'Supports Inline Queries', 'Is Premium', 'Added to Attachment Menu']

    for col_num, header in enumerate(headers, 1):
        ws.cell(row=1, column=col_num, value=header)

    # Запись данных о пользователях
    for row_num, user in enumerate(user_data, 2):
        for col_num, value in enumerate(user, 1):
            ws.cell(row=row_num, column=col_num, value=value)

    # Сохранение файла Excel
    wb.save('user_data.xlsx')

    send_excel_to_bot(adminId)

if __name__ == "__main__":
    print("Бот запустился")
    bot.polling(none_stop=True)