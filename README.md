# IP Info Bot (ipinfotelegrambot)

Этот Telegram-бот предоставляет информацию об IP-адресах и проверяет их валидность. Он разработан на языке Python с использованием библиотеки Telebot и API ipinfo.io для получения данных о IP-адресах.

## Использование

1. Начните чат с ботом: [@ipmalumotlaribot](https://t.me/ipmalumotlaribot) <!-- Замените на свое имя пользователя бота в Telegram -->
2. Отправьте боту IP-адрес в формате `X.X.X.X`.
3. Бот предоставит информацию о введенном IP-адресе, включая город, регион, страну и организацию (если доступно).

## Команды

- `/start` - Начать использование бота и получить информацию о его функциях.
- `/myip` - Узнать информацию о вашем IP.

## Установка и настройка

1. Клонируйте репозиторий:

```bash
git clone https://gitlab.com/Mr.Khakimov/ipinfotelegrambot.git
```

1. Войдите на директории проекта и установите зависимости питона:

```bash
cd ipinfotelegrambot
pip install
```
2. Создайте файл .env и добавьте свой API ключ бота:
```bash
BOT_TOKEN=your_bot_token_here
```

2. Создайте файл .env и добавьте свой API ключ бота:
```bash
BOT_TOKEN=your_bot_token_here
```


## Автор
Ваше имя Hakimov Hikmatullokh. Телеграм @mr.khakimov

Если у вас есть вопросы или предложения по улучшению бота, не стесняйтесь связаться с автором.

## Обязательно замените следующие элементы в файле `README.md`:

- `YourBotUsername` на имя пользователя вашего бота в Telegram.
- `https://t.me/YourBotUsername` на ссылку на вашего бота в Telegram.
- `yourusername/your-repo.git` на ссылку на ваш репозиторий на GitLab.
- `your_bot_token_here` на ваш API ключ бота.
- `[Ваше имя](https://gitlab.com/yourusername)` на ваш профиль на GitLab и ваше имя.